import numpy as np
import pandas as pd

sgd_usd_df = pd.read_csv('data/exchange-rates-sgd-per-unit-of-usd-daily.csv', index_col=0 \
      , parse_dates=['date'], infer_datetime_format=True)

print('Summary')
print(sgd_usd_df.describe())
print(sgd_usd_df.shape)

sgd_usd_df.insert(1, 'exchange_rate_EUR', 0)

print('Firs 10 rec')
print(sgd_usd_df.head(10))

jpy_df = pd.DataFrame(index=sgd_usd_df.index, columns=['exchange_rate_JPY'] \
      , dtype=np.float32, data=np.zeros(sgd_usd_df.shape[0]))

print('JPY Dataframe')
print(jpy_df.head(10))

result = pd.concat([sgd_usd_df, jpy_df])
print('Concat')
print('shape = ', result.shape)
print(result.head(10))

print('JYP = ', result.loc['1988-01-08', 'exchange_rate_JPY'])
print('USD = ', result.loc['1988-01-08', 'exchange_rate_usd'])
print('EUR = ', result.loc['1988-01-08', 'exchange_rate_EUR'])

print('jpy_df = ', jpy_df['exchange_rate_JPY'])

print(sgd_usd_df.shape)

result2 = sgd_usd_df.insert(1, 'exchange_rate_JPY', jpy_df['exchange_rate_JPY'])
print('Result 2')
print(result2)
