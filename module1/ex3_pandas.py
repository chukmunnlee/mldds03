import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

sgd_usd_df = pd.read_csv('data/exchange-rates-sgd-per-unit-of-usd-daily.csv', index_col=0 \
      , parse_dates=['date'], infer_datetime_format=True)

sgd_cny_df = pd.read_csv('data/sgd_cny_rates_daily.csv', index_col=0 \
      , parse_dates=['Date'], infer_datetime_format=True)

print(sgd_usd_df.info())
print('===================')
print(sgd_cny_df.info())

sgd_usd_cny_df = sgd_usd_df.join(sgd_cny_df)
sgd_usd_cny_dropna_df = sgd_usd_df.join(sgd_cny_df).dropna()

print('columns = ', sgd_usd_cny_df.columns)
print('first 10 = ', sgd_usd_cny_df.head(10))

plt.plot(sgd_usd_df, label='SGD/USD', color='blue')
plt.plot(sgd_cny_df, label='SGD/CNY', color='red')
plt.plot(sgd_usd_cny_df, label='SGD/USD/CNY', color='green')
#plt.plot(sgd_usd_cny_dropna_df, label='SGD/USD/CNY')

plt.show()
