import numpy as np
import pandas as pd

print('Panda version: %s\n' %pd.__version__)

sgd_usd_df = pd.read_csv('data/exchange-rates-sgd-per-unit-of-usd-daily.csv'
      , index_col=0, parse_dates=['date'], infer_datetime_format=True)

print('head(5) = ', sgd_usd_df.head(5))

print('index = ', sgd_usd_df.index)
print('index.name = ', sgd_usd_df.index.name)
print('index.ndim = ', sgd_usd_df.index.ndim)
print('index.shape = ', sgd_usd_df.index.shape)

print('values = ', sgd_usd_df.values)
print('values.ndim = ', sgd_usd_df.values.ndim)
print('values.shape = ', sgd_usd_df.values.shape)

print('columns = ', sgd_usd_df.columns)
print('info = ', sgd_usd_df.info())
print('count = ', sgd_usd_df.count())
print('sum = ', sgd_usd_df.sum())
print('sum[exchange_rate_usd] = ', sgd_usd_df.sum()['exchange_rate_usd'])
print('mean = ', sgd_usd_df.mean())
print('median = ', sgd_usd_df.median())

desc = sgd_usd_df.describe()

print('type = ', type(desc))
print(desc)

