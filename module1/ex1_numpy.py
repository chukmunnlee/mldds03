import requests
import numpy as np
from PIL import Image
from matplotlib import pyplot as plt

#url = 'https://images-na.ssl-images-amazon.com/images/I/71PT8q%2BWcGL._SY450_.jpg'
url = 'https://i.pinimg.com/736x/83/a3/73/83a3735c13754d3e3796d1e1e662864d--robots-vintage-vintage-toys.jpg'
url = 'https://banner2.kisspng.com/20180418/fre/kisspng-mecha-anime-super-robot-drawing-robot-5ad76e2c941fd0.9529665915240678846067.jpg'
url = 'https://i.ytimg.com/vi/ADzChcIW23o/hqdefault.jpg'

robbie = Image.open(requests.get(url, stream=True).raw)

tensor = np.array(robbie)

print('shape = ', tensor.shape)
print('dimension = ', tensor.ndim)

fig = plt.figure()
subplot = []
ax = fig.add_subplot(221)
ax.imshow(tensor)

for i in range(3):
   subplot.append(fig.add_subplot(222 + i))
   subplot[i].imshow(tensor[:, :, i])

plt.show()
