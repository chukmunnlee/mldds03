import sklearn

import numpy as np
import pandas as pd

from matplotlib import pyplot as plt
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error, mean_squared_log_error, r2_score

x_gdp = pd.read_csv('data/income-components-of-gross-domestic-product-at-current-prices-annual.csv' \
      , index_col=0, parse_dates=['year'], infer_datetime_format=True)

x_fresv = pd.read_csv('data/total-foreign-reserves-annual.csv' \
      , index_col=0, parse_dates=['year'], infer_datetime_format=True)

x_gdp_fresv = x_gdp.join(x_fresv)

emp_resv = x_gdp_fresv[x_gdp_fresv['level_2'] == 'Compensation Of Employees'].dropna()

print('time: x_gdp = ', x_gdp.index.values.min(), x_gdp.index.values.max())
print('time: x_fresv = ', x_fresv.index.values.min(), x_fresv.index.values.max())

emp_comp = emp_resv['value']
#fresv = emp_resv['total_foreign_reserve_sgd'].reshape(-1, 1)
fresv = emp_resv['total_foreign_reserve_sgd'].values[:, None]

print('emp_comp.shape = ', emp_comp.shape)
print('fresv.shape = ', fresv.shape)

model = LinearRegression()
model.fit(fresv, emp_comp)

print('coeff = ', model.coef_)
print('intercept = ', model.intercept_)

yhat = model.predict(fresv)

print('MSE = ', mean_squared_error(emp_comp, yhat))
print('MSLE = ', mean_squared_log_error(emp_comp, yhat))
print('R2 score = ', r2_score(emp_comp, yhat))

#
ax = plt.figure().add_subplot(111)
ax.scatter(emp_resv.index, emp_resv['value'], label="emp comp")
ax.scatter(emp_resv.index, emp_resv['total_foreign_reserve_sgd'], color='red', label="reserved")
ax.plot(emp_resv.index, yhat, color='green', label="model")
ax.set_xlabel('Year')

plt.legend()

plt.show()
