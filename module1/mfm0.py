import sklearn

import numpy as np
import pandas as pd

from matplotlib import pyplot as plt

x_gdp = pd.read_csv('data/income-components-of-gross-domestic-product-at-current-prices-annual.csv' \
      , index_col=0, parse_dates=['year'], infer_datetime_format=True)
x_gdp_level2 = x_gdp[x_gdp['level_2'] == 'Compensation Of Employees']

x_fresv = pd.read_csv('data/total-foreign-reserves-annual.csv' \
      , index_col=0, parse_dates=['year'], infer_datetime_format=True)
x_fresv_total_foreign_reserve_sgd = x_fresv['total_foreign_reserve_sgd']


#print(x_level2.values[:,-1])

print('time: x_gdp = ', x_gdp.index.values.min(), x_gdp.index.values.max())
print('time: x_fresv = ', x_fresv.index.values.min(), x_fresv.index.values.max())

print('x_gdp.index.values.shape = ', x_gdp.index.values.shape)
print('x_gdp.index.unique().shape = ', x_gdp.index.unique().shape)
print('x_gdp.index.is_unique = ', x_gdp.index.is_unique)
print('x_gdp_level2.values.shape = ', x_gdp_level2.values.shape)

plt.scatter(x_gdp.index.unique().values, x_gdp_level2.values[:, -1], color='green')

plt.plot(x_fresv.index.values, x_fresv_total_foreign_reserve_sgd, color='blue')

plt.show()
