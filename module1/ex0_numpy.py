import numpy as np
import pandas as pd
import requests
from matplotlib import pyplot as plt
from PIL import Image

sgd_usd = pd.read_csv('./data/exchange-rates-sgd-per-unit-of-usd-daily.csv'
      , parse_dates=True, index_col=0, infer_datetime_format=True
      , squeeze=True)

#print('sgd_usd = ', sgd_usd)

v = sgd_usd.values

print('values = ', v)
print('values.T = ', np.matrix(v).T)

print(sgd_usd.head(2))

print('Rank: number of dimension ', np.array(v).ndim)

print('Shape = ', v.shape)
print('Rank = ', v.ndim)
print('Rank = ', v.dtype)

matrix = np.array((v, v))
print('matrix = ', matrix)

concat_array = np.concatenate((v, v))
print('len(v) = ', len(v))
print('concat = ', concat_array)
print('len(concat) = ', len(concat_array))

print('matrix.shape = ', matrix.shape)

vstack_matrix = np.vstack((matrix, matrix))
print('vstack_matrix.shape = ', vstack_matrix.shape)

hstack_matrix = np.hstack((matrix, matrix))
print('hstack_matrix.shape = ', hstack_matrix.shape)


url = 'https://upload.wikimedia.org/wikipedia/commons/8/81/Singapore_Merlion_BCT.jpg';
image = Image.open(requests.get(url, stream=True).raw)

tensor = np.array(image)
print('shape = ', tensor.shape)
height = tensor.shape[0] //2
width = tensor.shape[1]//2

#print('height = %d, width = %d' %(height, width))

plt.imshow(tensor[:height][:width][:])
#plt.imshow(tensor)
#plt.axis('off')
plt.show()
