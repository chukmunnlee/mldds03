import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

motor_vehicle_pd = pd.read_csv('data/annual-motor-vehicle-population-by-vehicle-type.csv' \
      , parse_dates=['year'],  infer_datetime_format=True, index_col=0)

#print('index = ', motor_vehicle_pd.index)

cat_group = motor_vehicle_pd.groupby(motor_vehicle_pd.category)

for k, v in cat_group:
   print('cat: %s, total: %d' %(k, v['number'].sum()))

cat_group.sum().hist()

plt.show()

"""
grouped = motor_vehicle_pd.groupby(motor_vehicle_pd.index)

total = 0
for k, v in grouped:
   total += v['number'].sum()
   print('year: %s, total: %d' %(str(k), v['number'].sum()))
   #print(v['number'])

print('grouped sum: ', sum(list(grouped['number'].sum())))
print('sum of parts: %d' %total)

ax = motor_vehicle_pd.groupby(motor_vehicle_pd.index).sum().plot(marker='o')
ax.set_xlabel('year')
ax.set_ylabel('vehicle popluation')
ax.set_title('annual vehicle population')

plt.show()
"""
