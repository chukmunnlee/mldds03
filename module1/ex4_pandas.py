import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

sgd_per_usd_df = pd.read_csv('data/exchange-rates-sgd-per-unit-of-usd-average-for-period-annual.csv' \
      , parse_dates=['year'], infer_datetime_format=True)

sgd_usd_df = pd.read_csv('data/exchange-rates-sgd-per-unit-of-usd-daily.csv', index_col=0 \
      , parse_dates=['date'], infer_datetime_format=True)

print('sgd_per_usd_df.info(): ', sgd_per_usd_df.info())
print('sgd_per_usd_df.describe(): ', sgd_per_usd_df.describe())

print('sgd_per_usd_df.columns: ', sgd_per_usd_df.columns)
print('sgd_per_usd_df.index: ', list(sgd_per_usd_df.index))

print('sgd_usd_df.index: ', sgd_usd_df.index)

print('sgd_per_usd_df.head(5): ', sgd_per_usd_df.head(5))

print('sgd_usd_df.head(5): ', sgd_per_usd_df.head(5))

#print(sgd_per_usd_df.head(5).iloc[0:5])

#print('===================', sgd_per_usd_df.year)

print(sgd_usd_df.head(5).loc['1988-01-08', 'exchange_rate_usd'])

#for i in sgd_per_usd_df.head(5).iterrows():
   #print('sgd_per_unit_of_usd: ', i['sgd_per_unit_of_usd'])
   #print('sgd_per_unit_of_usd: ', i[1].iloc[0:3])

