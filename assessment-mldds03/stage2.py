import pickle
import numpy as np
import pandas as pd

from sklearn.linear_model import SGDClassifier
from sklearn.dummy import DummyRegressor

X_scaled_df, Y_encoded_df, le, scaler = pickle.load(open('data/stage1.pickle', 'rb'))

"""
#score =  0.6265486725663717
sgd_classifier = SGDClassifier(random_state=42, loss='squared_loss', penalty='l2', \
      verbose=True, max_iter=5000)
"""

# score = 0.9911504424778761
sgd_classifier = SGDClassifier(random_state=42, \
      verbose=True, max_iter=2000)
sgd_classifier.fit(X_scaled_df, Y_encoded_df)

dummy_regressor = DummyRegressor()
dummy_regressor.fit(X_scaled_df, Y_encoded_df)

pickle.dump((sgd_classifier, dummy_regressor), open('data/stage2.pickle', 'wb'))
