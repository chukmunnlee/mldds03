import sys, pickle
import numpy as np 
import pandas as pd 

from sklearn.preprocessing import LabelEncoder

tr_df = pd.read_csv('data/training_data.csv')

tr_df.dropna(inplace=True)

tr_df.drop(['id'], inplace=True, axis=1)

"""
[  'diagnosis', 'radius_mean', 'texture_mean', 'perimeter_mean',
   'area_mean', 'smoothness_mean', 'compactness_mean', 'concavity_mean',
   'concave points_mean', 'symmetry_mean', 'fractal_dimension_mean',
   'radius_se', 'texture_se', 'perimeter_se', 'area_se', 'smoothness_se',
   'compactness_se', 'concavity_se', 'concave points_se', 'symmetry_se',
   'fractal_dimension_se', 'radius_worst', 'texture_worst',
   'perimeter_worst', 'area_worst', 'smoothness_worst',
   'compactness_worst', 'concavity_worst', 'concave points_worst',
   'symmetry_worst', 'fractal_dimension_worst']
"""

Y_df = tr_df.loc[:, ['diagnosis']]
tr_df.drop(['diagnosis'], inplace=True, axis=1)

pickle.dump((tr_df, Y_df), open('data/stage0.pickle', 'wb'))

