import pickle

import numpy as np
import pandas as pd

import seaborn as sns

from matplotlib import pyplot as plt

from sklearn.metrics import classification_report, confusion_matrix, roc_curve, auc

X_scaled_df, Y_encoded_df, le, scaler = pickle.load(open('data/stage1.pickle', 'rb'))
sgd_classifier, dummy_regressor = pickle.load(open('data/stage2.pickle', 'rb'))

test_df = pd.read_csv('data/prediction_data.csv')

test_df.dropna(inplace=True)
test_df.drop(['id'], inplace=True, axis=1)

Y_test_encoded_df = le.transform(test_df.loc[:, ['diagnosis']].values.ravel())

test_df.drop(['diagnosis'], inplace=True, axis=1)

X_test_scaled_df = scaler.transform(test_df)

Y_sgd_predict = sgd_classifier.predict(X_test_scaled_df)

Y_sgd_prob = sgd_classifier.decision_function(X_test_scaled_df)

# metrics
labels = le.inverse_transform(sgd_classifier.classes_)
## classification_report
print(classification_report(Y_test_encoded_df, Y_sgd_predict, \
      target_names=labels))

## confusin matrix
cm = confusion_matrix(Y_test_encoded_df, Y_sgd_predict)

fig = plt.figure(1)
ax = fig.add_subplot(121)

sns.heatmap(cm, annot=True, ax=ax, fmt='d')

ax.set_xlabel('Prediction')
ax.set_ylabel('Truth')
ax.set_title('Confusion Matrix')
ax.xaxis.set_ticklabels(labels)
ax.yaxis.set_ticklabels(labels)

## roc curve
fpr, tpr, _ = roc_curve(Y_test_encoded_df, Y_sgd_prob)
ay = fig.add_subplot(122)

ay.plot(fpr, tpr, label='auc %f' %auc(fpr, tpr))
ay.set_xlabel('False positive rate')
ay.set_ylabel('True positive rate')
ay.set_title('ROC curve')

plt.show()
