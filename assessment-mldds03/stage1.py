import pickle
import numpy as np
import pandas as pd

from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import axes3d

from sklearn.decomposition import PCA

from sklearn.preprocessing import LabelEncoder, StandardScaler

X_df, Y_df = pickle.load(open('data/stage0.pickle', 'rb'))

le = LabelEncoder()
Y_encoded_df = le.fit_transform(Y_df.values.ravel())

scaler = StandardScaler()
X_scaled_df = scaler.fit_transform(X_df)

pickle.dump((X_scaled_df, Y_encoded_df, le, scaler), open('data/stage1.pickle', 'wb'))

"""
pca = PCA(n_components=2)
X_2d = pca.fit_transform(X_scaled_df)

plt.title('PCA n_dimensions=2')
plt.scatter(X_2d[:, 0], X_2d[:, 1], s=2)

pca = PCA(n_components=3)
X_3d = pca.fit_transform(X_scaled_df)

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

ax.scatter(X_3d[:, 0], X_3d[:, 1], X_3d[:, 2], s=2)

plt.title('PCA n_dimensions=3')

plt.show()
"""
