import pickle
import numpy as np
import pandas as pd

from sklearn.preprocessing import LabelEncoder

import spacy
from spacy.lemmatizer import Lemmatizer 
from spacy.lang.en import LEMMA_INDEX, LEMMA_EXC, LEMMA_RULES

def tokenize(text):
   doc = nlp(text)
   lemmas = [ lemmatizer(token.text, token.pos_) for token in doc \
         if not token.is_stop and token.is_alpha ]
   return ' '.join([ word for stmt in lemmas for word in stmt ])

jobs_df = pd.read_csv('data/job_skills.csv')

jobs_df.dropna(inplace=True)

category_df = pd.DataFrame(jobs_df.Category, columns=['Category'])
responsibilties_df = pd.DataFrame(jobs_df.Responsibilities, columns=['Responsibilities'])

category_encoder = LabelEncoder()
category_encoder.fit(category_df.values.ravel())
category_encoded_df = pd.DataFrame(category_encoder.transform(category_df.values.ravel()), \
      columns=['Category'])

nlp = spacy.load('en_core_web_sm')
lemmatizer = Lemmatizer(LEMMA_INDEX, LEMMA_EXC, LEMMA_RULES)

print('Start tokenizing')
responsibilties_tokenized_df = responsibilties_df.Responsibilities.apply(tokenize)

pickle.dump((category_encoded_df, responsibilties_tokenized_df, category_encoder), \
      open('data/ngram0.pickle', 'wb'))
