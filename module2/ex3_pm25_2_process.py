import sys
import numpy as np
import pandas as pd
import pickle

from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import SGDRegressor, LinearRegression
from sklearn.metrics import mean_squared_error, mean_squared_log_error, r2_score

from matplotlib import pyplot as plt

SGD_ITERATION = 2000
csv_file = 'data/processed_PRSA_data_2010.1.1-2014.12.31.csv' \
      if len(sys.argv) <= 1 else sys.argv[1]

#read file
pm25_df = pd.read_csv(csv_file, index_col=0 \
      , infer_datetime_format=True, parse_dates=['date_time'])

#isolate pm2.5 column to a dataframe
y_pm25 = pm25_df.loc[ :, 'pm2.5']
#everything else to another dataframe
X_pm25 = pm25_df.loc[ :, ~pm25_df.columns.isin(['pm2.5', 'date_time'])]

#split data
X_train, X_test, y_train, y_test = train_test_split(X_pm25, y_pm25)

print('train: x=%d, y=%d' %(len(X_train), len(y_train)))
print('test: x=%d, y=%d' %(len(X_test), len(y_test)))

scaler_x = StandardScaler()
scaler_y = StandardScaler()

scaler_x.fit(X_train)
X_scaled_train = pd.DataFrame(scaler_x.transform(X_train))

scaler_y.fit(y_train[:, None])
y_scaled_train = pd.DataFrame(scaler_y.transform(y_train[:, None]))

X_scaled_test = pd.DataFrame(scaler_x.transform(X_test))
y_scaled_test = pd.DataFrame(scaler_y.transform(y_test[:, None]))

print('train scaled: x=%d, y=%d' %(len(X_scaled_train), len(y_scaled_train)))
print('test scaled: x=%d, y=%d' %(len(X_scaled_test), len(y_scaled_test)))

sgdr = SGDRegressor(n_iter=SGD_ITERATION, verbose=True)
sgdr.fit(X_scaled_train, y_scaled_train)

pickle.dump(( X_scaled_test, y_scaled_test, scaler_x, scaler_y, sgdr ) \
      , open('data/sgd_regressor-%d.pickle' %SGD_ITERATION, 'wb'))

y_predict_train = sgdr.predict(X_scaled_train)
y_predict_test = sgdr.predict(X_scaled_test)

print('Train: MSE = ', mean_squared_error(y_scaled_train, y_predict_train))
print('Train: R2 score = ', r2_score(y_scaled_train, y_predict_train))

print('Test: MSE = ', mean_squared_error(y_scaled_test, y_predict_test))
print('Test: R2 score = ', r2_score(y_scaled_test, y_predict_test))

"""
fig = plt.figure()

train_err = y_predict_train - y_scaled_train.iloc[0].values
test_err = y_predict_test - y_scaled_test.iloc[0].values

ax = fig.add_subplot(121)
ax.scatter(np.arange(len(X_scaled_train)), y_scaled_train, label="actual", color="green", s=1)
ax.scatter(np.arange(len(X_scaled_train)), y_predict_train, label="predict", color="red", s=1)
ax.plot(np.arange(len(X_scaled_train)), train_err, label="error", color="yellow")
ax.set_title('Train')

ay = fig.add_subplot(122)
ay.scatter(np.arange(len(X_scaled_test)), y_scaled_test, label="actual", color="green", s=1)
ay.scatter(np.arange(len(X_scaled_test)), y_predict_test, label="predict", color="red", s=1)
ay.plot(np.arange(len(X_scaled_test)), test_err, label="error", color="yellow")
ay.set_title('Test')

plt.legend()

plt.show()
"""
