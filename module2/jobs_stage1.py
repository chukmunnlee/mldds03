import pickle
import numpy as np
import pandas as pd

from matplotlib import pyplot as plt

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.manifold import TSNE

category_encoded_df, responsibilties_tokenized_df, _ = pickle.load( \
      open('data/jobs_stage0.pickle', 'rb'))

#tfidf_vectorizer = TfidfVectorizer(lowercase=False, decode_error='ignore')
tfidf_vectorizer = TfidfVectorizer()
tfidf_vectorizer.fit(responsibilties_tokenized_df)

print('Vectorizing responsibilities tokens')
responsibilties_vectorized = tfidf_vectorizer.transform(responsibilties_tokenized_df)
responsibilties_vectorized_dense = responsibilties_vectorized.todense()

print('Done')

pickle.dump((responsibilties_vectorized, tfidf_vectorizer), open('data/jobs_stage1.pickle', 'wb'))

tsne = TSNE(n_components=2, random_state=42)
X_2d = tsne.fit_transform(responsibilties_vectorized_dense)

fig = plt.figure()

ax = fig.add_subplot(111)

ax.scatter(X_2d[:, 0], X_2d[:, 1], c=category_encoded_df.Category)

plt.grid()

plt.show()


