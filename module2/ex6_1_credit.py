import numpy as np
import pandas as pd
import pickle

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d

from sklearn.decomposition import PCA
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler

crx_onehot_df, y_df = pickle.load(open('data/crx_onehot.pickle', 'rb'))

X_train, X_test, y_train, y_test = train_test_split(crx_onehot_df, y_df)

print('train: ', len(X_train), len(y_train))
print('test: ', len(X_test), len(y_test))

scaler = StandardScaler()
scaler.fit(X_train)
X_train_scaled_df = pd.DataFrame(scaler.transform(X_train))

#print('scaler mean = ', scaler.mean_)
#print('crx_scaled_df  = ', X_train_scaled_df.head(5))

pca = PCA(n_components=2)
comp2 = pca.fit_transform(X_train_scaled_df)

print(comp2.shape)

fig = plt.figure()

ax = fig.add_subplot(111, projection='3d')

print(comp2.shape)
print(y_train.shape)
print('>> y_train ', (y_train==1).index)
print('>> comp2 ', comp2[y_train==1])

#ax.scatter(comp2[y_train==0, 0], comp2[y_train==1, 1], y_train)
ax.scatter(comp2[y_train==0, 0], comp2[y_train==1, 1], y_train, alpha=0.5, color='b')

plt.show()
