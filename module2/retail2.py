import numpy as np
import pandas as pd

from matplotlib import pyplot as plt

from sklearn.preprocessing import StandardScaler
from sklearn.cluster import KMeans 

online_df = pd.read_csv('data/online_combined.csv')

cluster_columns = [ 'StockCode', 'Quantity', 'UnitPrice', 'Country', ]
selected_df = online_df.loc[:, cluster_columns]

scaler = StandardScaler()
selected_scaled_pd = scaler.fit_transform(selected_df)

inertias = []

for grp in range(2, 40):
   print('Clustering %d' %grp)
   kmeans = KMeans(n_clusters=grp)
   kmeans.fit(selected_scaled_pd)
   inertias.append(kmeans.inertia_)

plt.plot(range(2, 40), inertias, marker='o')
plt.grid()
plt.show()

