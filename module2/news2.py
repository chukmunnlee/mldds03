import pickle
import numpy as np
import pandas as pd

from matplotlib import pyplot as plt 

from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.dummy import DummyClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import classification_report, roc_curve, auc

relevance_encoded, headline_tokens_df, encoder_relevance = pickle.load( \
      open('data/news0.pickle', 'rb'))

X_headline_train_df, y_relevance_train_df, X_headline_test_df, y_relevance_test_df, tfidf_vectorizer = \
      pickle.load(open('data/news1.pickle', 'rb'))

relevance_encoded_df = pd.DataFrame(relevance_encoded)
relevance_encoded_df.columns = [ 'relevance' ]

X_headline_train_dense = tfidf_vectorizer.transform(X_headline_train_df).todense()
X_headline_test_dense = tfidf_vectorizer.transform(X_headline_test_df).todense()

classifiers = [ \
      DummyClassifier(random_state=42), \
      LogisticRegression(random_state=42), \
      #LogisticRegression(random_state=42, class_weight={0: 0.2, 1: 1.5}), \
      #KNeighborsClassifier(n_neighbors=3) \
      ]

#fit classifiers
for cls in classifiers:
   print('Fitting %s' %cls.__class__)
   cls.fit(X_headline_train_dense, y_relevance_train_df.values.ravel())

fig = plt.figure()
ax = fig.add_subplot(121)
ay = fig.add_subplot(122)
for cls in classifiers:
   if hasattr(cls, 'decision_function'):
      y_confidence = cls.decision_function(X_headline_test_dense)
      fpr0, tpr0, treshold = roc_curve(y_relevance_test_df, y_confidence, pos_label=0)
      fpr1, tpr1, treshold = roc_curve(y_relevance_test_df, y_confidence, pos_label=1)

      for i in range(len(fpr1)):
         print(fpr0[i], '\t\t', treshold[i])
      break
   else:
      y_confidence = cls.predict_proba(X_headline_test_dense)
      fpr0, tpr0, treshold = roc_curve(y_relevance_test_df, y_confidence[:, 0], pos_label=0)
      fpr1, tpr1, treshold = roc_curve(y_relevance_test_df, y_confidence[:, 1], pos_label=1)

"""
   ax.plot(fpr1, tpr1, label='%s (area = %s)' %(cls.__class__, auc(fpr1, tpr1)))
   ax.set_title('*** Relevant')
   ax.grid()
   ay.plot(fpr0, tpr0, label='%s (area = %s)' %(cls.__class__, auc(fpr0, tpr0)))
   ay.set_title('*** Irrelevant')
   ay.grid()

plt.legend()

plt.show()
"""
