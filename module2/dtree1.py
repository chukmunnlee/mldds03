import numpy as np
import pandas as pd

from sklearn.model_selection import train_test_split

target_cols=['Page total likes', 'Type', 'Category', 'Post Month', \
      'Post Weekday', 'Post Hour', 'Paid', 'Lifetime Post Total Reach']

usecols=['Page total likes', 'Type', 'Category', 'Post Month', \
      'Post Weekday', 'Post Hour', 'Paid', 'Lifetime Post Total Reach']

df = pd.read_csv('data/dataset_Facebook.csv', delimiter=';', usecols=usecols)

df.dropna(inplace=True)

##Balance the tree - Type Photo is skewed
print('Type = \n', df['Type'].value_counts())
print('Category = \n', df['Category'].value_counts())
print('Paid = \n', df['Paid'].value_counts())

num_of_photos = 90
photo_df = df.loc[df.Type == 'Photo']

to_keep = photo_df.sample(num_of_photos).index
to_drop = list(set(photo_df.index) - set(to_keep))
df_bal = df.drop(df.index[to_drop])

data = df_bal[target_cols]
print(data.columns)

type_dummies = pd.get_dummies(data['Type'], prefix="Type")

df_with_dummies = pd.concat([data, type_dummies], axis=1)
print(df_with_dummies.columns)

target = df_with_dummies['Lifetime Post Total Reach']

X_train, X_test, y_train, y_test = train_test_split(df_with_dummies, target)
