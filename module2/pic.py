import requests

import numpy as np
import pandas as pd

from sklearn.cluster import KMeans
from sklearn.decomposition import PCA
from sklearn.utils import shuffle

from matplotlib import pyplot as plt

from PIL import Image

url = 'https://i.ytimg.com/vi/ADzChcIW23o/hqdefault.jpg'

robot = Image.open(requests.get(url, stream=True).raw)

"""
(360, 480, 3)
"""
robot_arr = np.array(robot)

w, h, d = robot_arr.shape

robot_2d = robot_arr.reshape(w * h, d) / 255
#robot_2d = robot_arr.reshape(w * h, d) 

sample_color = shuffle(robot_2d, random_state=42)[:1000]

fig = plt.figure()
subplot = 250

for c in range(1, 10):

   subplot += 1

   kmeans = KMeans(n_clusters=c, random_state=42)
   kmeans.fit(sample_color)

   labels = kmeans.predict(robot_2d)

   compressed = np.zeros((w, h, d))
   idx = 0
   for i_w in range(w):
      for j_h in range(h):
         #compressed[i_w, j_h] = np.floor(kmeans.cluster_centers_[labels[idx]], dtype=int)
         compressed[i_w, j_h] = kmeans.cluster_centers_[labels[idx]]
         idx += 1

   print(subplot)
   ax = fig.add_subplot(subplot)
   ax.set_title('Cluster size: %d' %c)
   ax.imshow(compressed)

plt.show()
