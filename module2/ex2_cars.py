import numpy as np
import pandas as pd

from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from sklearn.model_selection import train_test_split, learning_curve
from sklearn.linear_model import LinearRegression, SGDRegressor
from sklearn.metrics import mean_squared_error, mean_squared_log_error, r2_score

from scipy.interpolate import griddata

from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import axes3d

columns = [ 'mpg', 'cylinders', 'displacement', 'horsepower', 'weight' \
      , 'acceleration', 'model_year', 'origin', 'car_name' ]
na_values = [ '?' ]

cars = pd.read_csv('data/auto-mpg.data.txt', names = columns \
      , delim_whitespace=True, na_values=na_values)
#cars.columns = columns
cars.dropna(inplace=True)

X = cars.iloc[:, 1: 6]
y = cars.mpg

#split data to train and test
train_x, test_x, train_y, test_y = train_test_split(X, y)

#scale x and y. Use 2 scaler so they don't interfer with each other

# standard scaler scales between -1 and 1
x_train_scaler = StandardScaler()
y_train_scaler = StandardScaler()

x_train_scaler.fit(train_x)
x_train_scaled = pd.DataFrame(x_train_scaler.transform(train_x))

y_train_scaler.fit(train_y[:, None])
y_train_scaled = pd.DataFrame(y_train_scaler.transform(train_y[:, None]))

print('train: ', x_train_scaled.shape, y_train_scaled.shape)


"""
IMPORTANT
use the same scale for train and test. Scale the test first. Once fitted
we use the mean/median of the test to scale the test
"""

x_train_scaler.fit(test_x)
x_test_scaled = pd.DataFrame(x_train_scaler.transform(test_x))

y_train_scaler.fit(test_y[:, None])
y_test_scaled = pd.DataFrame(y_train_scaler.transform(test_y[:, None]))

print('test: ', x_test_scaled.shape, y_test_scaled.shape)

print(y_train_scaled.shape)

lr = LinearRegression()
lr.fit(x_train_scaled, y_train_scaled)
result = lr.predict(x_test_scaled)

print('x_test_scaled = ', x_test_scaled.shape)
print('result = ', result.shape)
print('y_test_scaled = ', y_test_scaled.shape)

print('LR MSE = ', mean_squared_error(y_test_scaled, result))
print('LR r2 score = ', r2_score(y_test_scaled, result))

sgdr = SGDRegressor(learning_rate='constant', verbose=False, n_iter=1000)

sgdr.fit(x_train_scaled, y_train_scaled)
result = sgdr.predict(x_test_scaled)

print('SGD MSE = ', mean_squared_error(y_test_scaled, result))
print('SGD r2 score = ', r2_score(y_test_scaled, result))

rec_input = np.array(list(x_test_scaled.iloc[0]))

print(rec_input)
#print(rec_input[:, None].shape)

test_y_0 = sgdr.predict(rec_input[None, :])

print(test_y_0)
print(y_train_scaler.inverse_transform(test_y_0), test_y.iloc[0])

lr = LinearRegression()
train_size, train_score, validation_score = learning_curve(lr, x_train_scaled, y_train_scaled \
      , train_sizes=np.arange(.1, 1.1, .1))

print(train_size)
print(train_score)
print(validation_score)

train_mean = np.mean(train_score, axis=1)
validation_mean = np.mean(validation_score, axis=1)

plt.plot(train_size, train_mean, label='Train', color='green', marker='x')
plt.plot(train_size, validation_mean, label='Validation', color='yellow', marker='x')

plt.legend()

plt.show()

"""
x6 = cars.iloc[:, 1: 6]
x5 = cars.iloc[:, 1: 5]
y = cars.iloc[:, 0]

#reduce x number of components to 1
pca = PCA(n_components=1)
x_reduced_6 = pca.fit_transform(x6)

pca2 = PCA(n_components=2)
x_pca2 = pca2.fit_transform(x6)


fig = plt.figure()

ax = fig.add_subplot(121)
ax.scatter(x_reduced_6, y, color='yellow')

ay = fig.add_subplot(122, projection='3d')
ay.scatter(x_pca2[:, 0], x_pca2[:, 1], y)

plt.show()

x_low = np.min(x_pca2[:, 0])
x_max = np.max(x_pca2[:, 0])
y_low = np.min(x_pca2[:, 1])
y_max = np.max(x_pca2[:, 1])

print('x: min=%0.5f, max=%0.5f' %(x_low, x_max))
print('y: min=%0.5f, may=%0.5f' %(y_low, y_max))
print('z: min=%0.5f, may=%0.5f' %(min(y), max(y)))

xi = np.linspace(x_low, x_max, 20)
yi = np.linspace(y_low, y_max, 20)
zi = griddata((x_pca2[:, 0], x_pca2[:, 1]), y, (xi[None, :], yi[:, None]), method='cubic')

X, Y = np.meshgrid(xi, yi)

az = fig.add_subplot(223, projection='3d')
az.plot_wireframe(X, Y, zi, rstride=1, cstride=1)

plt.show()
"""

#del cars['car name']

#print(cars.head(10))
#print(cars.iloc[10:20])
#print(cars.iloc[10:20, 1:5])

#print the column types
#print(cars.dtypes)

#print(cars.describe())

#print(cars.loc[cars['origin'] > 1, ['car_name']])
