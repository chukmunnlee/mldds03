import numpy as np
from matplotlib import pyplot as plt 

def plot_decision_boundaries(n_iter, plot, kmeans, data):
   h = 0.02
   BAZEL = 0.5

   # Plot the decision boundary
   x_min, x_max = data[:, 0].min() - BAZEL, data[:, 0].max() + BAZEL
   y_min, y_max = data[:, 1].min() - BAZEL, data[:, 1].max() + BAZEL
   xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))

   # Obtain labels for each point in the mesh using the trained model
   Z = kmeans.predict(np.c_[xx.ravel(), yy.ravel()])
   # Put the result into a color plot
   Z = Z.reshape(xx.shape)

   plot.imshow(Z, interpolation='nearest', \
         extent=(xx.min(), xx.max(), yy.min(), yy.max()), \
         cmap=plt.cm.Pastel2, aspect='auto', origin='lower')

   plot.plot(data[:, 0], data[:, 1], 'k.', markersize=4)

   #Plot the cetroids
   centroids = kmeans.cluster_centers_

   plot.scatter(centroids[:, 0], centroids[:, 1], marker='x', s=169, linewidths=3, \
         color='red', zorder=10, label='centroids')

   plot.set_title('K-means, %d iterations(s)' %n_iter)
   plot.set_xlim(x_min, x_max)
   plot.set_ylim(y_min, y_max)

