import sys
import numpy as np
import pandas as pd

file = 'data/PRSA_data_2010.1.1-2014.12.31.csv'
clean_csv = 'data/processed_PRSA_data_2010.1.1-2014.12.31.csv'

#read data
pm25_df = pd.read_csv(file, index_col=0)
pm25_nafree_df = pm25_df.dropna()

pm25_na_df = pm25_df[~pm25_df.index.isin(pm25_nafree_df.index)]

dropped_rows = (len(pm25_na_df)/len(pm25_df)) / 100

print('%0.5f%% dropped' %dropped_rows)

#reindex - better
pm25_nafree_df.index = pd.to_datetime({ \
      'year': pm25_nafree_df.year, 'month': pm25_nafree_df.month \
      , 'day': pm25_nafree_df.day, 'hour': pm25_nafree_df.hour })
pm25_nafree_df.index.name = 'date_time'

#combine year, month and day columns to new date column
#pm25_nafree_df['date'] = [ '-'.join(i) for i in zip( pm25_nafree_df['year'].astype(str) \
      #, pm25_nafree_df['month'].astype(str), pm25_nafree_df['day'].astype(str)) ]
#map wind directions to numerical value
pm25_nafree_df['cbwd'] = pm25_nafree_df['cbwd'].map({ 'cv': 0, 'NE': 1, 'SE': 2, 'SW': 3, 'NW': 4 })

#drop year, month and day columns
pm25_clean_df = pm25_nafree_df.drop(['year', 'month', 'day', 'hour'], axis=1)

##write the file out
pm25_clean_df.to_csv(clean_csv)

print('Written %s' %clean_csv)
