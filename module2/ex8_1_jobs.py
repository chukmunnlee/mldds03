import numpy as np
import pandas as pd
import pickle

from sklearn.preprocessing import LabelEncoder
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.manifold import TSNE

from matplotlib import pyplot as plt

import spacy
from spacy.lemmatizer import Lemmatizer
from spacy.lang.en import LEMMA_INDEX, LEMMA_EXC, LEMMA_RULES

nlp = spacy.load('en')
lemmatizer = Lemmatizer(LEMMA_INDEX, LEMMA_EXC, LEMMA_RULES)

tokenized_df, Y_category_encoded_df = pickle.load(open('data/tokenized_df.pickle', 'rb'))

print('>>> ', Y_category_encoded_df.columns)

vectorizer = TfidfVectorizer(lowercase=False, decode_error='ignore')

X = vectorizer.fit_transform(tokenized_df)
X_dense = X.todense()

#print(vectorizer.get_feature_names())

print('X sparse = ', X.shape)
print('X dense = ', X_dense.shape)

tsne = TSNE(n_components=2, random_state=42)
X_2d = tsne.fit_transform(X_dense)

fig = plt.figure()

ax = fig.add_subplot(111)
ax.scatter(X_2d[:, 0], X_2d[:, 0], c=Y_category_encoded_df.Category)
ax.grid()

plt.show()
