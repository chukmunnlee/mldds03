import numpy as np
import pandas as pd
import pickle

crx_df = pd.read_csv('data/crx.data.csv' \
      , names = [ 'A%d' %i for i in range(1, 17) ] \
      , na_values=['?', 'nan'])

crx_df.dropna(inplace=True)

#A16 is our decision/label
#get A16 from df then drop the column
y = crx_df.loc[:, 'A16']
crx_df.drop(['A16'], inplace=True, axis=1)

#encode + and -
y_mapped = y.map({'+': 1, '-': 0})

"""
print('Columns: %s' %str(crx_df.columns))
print('data types = ', crx_df.dtypes)
print(crx_df.head(5))
"""

one_hot_cols = [ 'A1', 'A4', 'A5', 'A6', 'A7', 'A9', 'A10', 'A12', 'A13' ]

#to see what are the unique values
one_hot_df = pd.get_dummies(crx_df, columns=one_hot_cols)
print('columns = ', one_hot_df.columns)
print('head = ', one_hot_df.head(5))

#drop one_hot_cols
crx_df.drop(one_hot_cols, inplace=True, axis=1)

#combine the 2 DF to one
crx_onehot_df = pd.concat([crx_df, one_hot_df], axis=1)

pickle.dump((crx_onehot_df, y_mapped), open('data/crx_onehot.pickle', 'wb'))
