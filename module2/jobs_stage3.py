import pickle
import numpy as np
import pandas as pd

from sklearn.preprocessing import LabelEncoder
from sklearn.linear_model import LinearRegression, SGDRegressor
from sklearn.feature_extraction.text import TfidfVectorizer

import spacy
from spacy.lemmatizer import Lemmatizer
from spacy.lang.en import LEMMA_INDEX, LEMMA_EXC, LEMMA_RULES

_, _, category_encoder = pickle.load(open('data/jobs_stage0.pickle', 'rb'))

category_encoded_df, tfidf_vectorizer, lr, sgdregressor = pickle.load( \
      open('data/jobs_stage2.pickle', 'rb'))

nlp = spacy.load('en_core_web_sm')
lemmatizer = Lemmatizer(LEMMA_INDEX, LEMMA_EXC, LEMMA_RULES)

def tokenize(text):
   doc = nlp(text)
   lemmas = [ lemmatizer(token.text, token.pos_) for token in doc \
         if not token.is_stop and token.is_alpha ]
   return ' '.join([ word for sentence in lemmas for word in sentence ])


"""
print(category_df.Category.unique())
['Program Management' 'Manufacturing & Supply Chain' 'Technical Solutions'
 'Developer Relations' 'Hardware Engineering' 'Partnerships'
 'Product & Customer Support' 'Software Engineering'
 'IT & Data Management' 'Business Strategy' 'Technical Writing'
 'Technical Infrastructure' 'Marketing & Communications'
 'Sales & Account Management' 'Sales Operations' 'Finance'
 'Legal & Government Relations' 'Administrative'
 'User Experience & Design' 'People Operations'
 'Real Estate & Workplace Services']
"""

job_descp = [ \
         """Partner closely with Engineering, Sales, Marketing, Finance and other product teams across Google to understand their vision, objectives and opportunities for strategic alignment.  Define overall strategy, product roadmap and technical/feature specifications for new products based on long-term product vision, user needs, technical and market trends, and platform capabilities.  Manage product life-cycle from inception through to execution, productization and End of Life.  Manage the product execution working closely with Engineering, R&D, Industrial Design, Product Design and partner teams to engineer the best experience for our users.  Evaluate mechanical design, CMF and production decisions as part of the product development lifecycle.  """, \
         """Work on a fast-paced, rapidly growing, high-profile project with a significant opportunity for industry-level impact on emerging container-based technologies.  Devise innovative ideas for solving Cloud customer problems and translate these ideas into technical designs.  Provide technical leadership and solve end-to-end the most difficult problems (work on design, implementation and productionization).  Be a hands-on coder applying the best industry standards for code health, scalability and robustness, mentoring more junior Software Engineers.  Collaborate with technical leaders working on shaping Kubernetes product within Google and within the open source community worldwide, contribute to product and project strategy, roadmap definition, and requirements gathering""" , \
         """Work with Android partners to drive adoption of product integrations across Google's platforms.
Provide 1-1 engineering direction and support to design, build and launch new feature integrations with partners.
Propose the integration of new features which change how users interact with apps and Google, and help partners integrate them into their product/services.
Take regular, engineering-focused meetings with partners to help them design new systems, fix bugs, improve UX and solve complex code challenges at a scale of millions of users.
Work on the core source code of Google's products with other engineers to identify, reproduce and/or fix bugs that are affecting top partners.""", \
      """Work closely with strategic clients, both engineering and non-technical, to lead migration projects and customer implementations on Google Cloud.
Coordinate with a diverse team of stakeholders and supporting Googlers, including Sales, Solutions Engineers and the Professional Services organization.
Build core migration tooling across all Google Cloud Platform products and relevant third-party software.
Establish and drive planning and execution steps towards production deployments.
Write/develop deployment templates, orchestration scripting, database replication configurations, CI/CD pipeline assemblies, etc."""
      ]

for jd in job_descp:
   tokens = tokenize(jd)

   tokens_vector = tfidf_vectorizer.transform([tokens])

   predicted_category = lr.predict(tokens_vector)

   print('Predicted category = %d, %s' \
         %(predicted_category, category_encoder.inverse_transform(predicted_category[0])))

