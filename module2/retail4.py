import numpy as np
import pandas as pd

from sklearn.cluster import KMeans
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split

from plot_decision_boundaries import plot_decision_boundaries

CLUSTERS = 5
ITERATIONS = 20

online_df = pd.read_csv('data/online_combined.csv')

cluster_columns = [ 'StockCode', 'Quantity', 'UnitPrice', 'Country', ]
selected_df = online_df.loc[:, cluster_columns]

X_train, X_test = train_test_split(selected_df)

print('train = ', len(X_train))
print('test = ', len(X_test))

scaler = StandardScaler()

kmeans = KMeans(n_clusters=CLUSTERS, random_state=42)
kmeans.fit(X_train)
