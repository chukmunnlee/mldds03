import sys
import numpy as np
import pandas as pd

from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from scipy.sparse import csc_matrix, csr_matrix


file = 'data/PRSA_data_2010.1.1-2014.12.31.csv'

pm25_df = pd.read_csv(file)

#pm25_df.dropna(inplace=True)

le = LabelEncoder()
le.fit(pm25_df.cbwd)

class_size = len(le.classes_)

encoded_cbwd = le.transform(pm25_df.cbwd)

onehot = []

for i in encoded_cbwd:
   r = np.zeros(class_size)
   r[i] = 1
   onehot.append(r)

onehot_df = pd.DataFrame(onehot, columns=['oh_%s' %i for i in le.classes_])

pm25_with_onehot_df = pd.concat([pm25_df, onehot_df], axis=1)

#Faster way of getting a one hot labels
onehot_cbwd = pd.get_dummies(pm25_df.cbwd)
print(onehot_cbwd)

"""
for i in range(len(pm25_with_onehot_df)):
   print('> %d ' %i, pm25_with_onehot_df.iloc[i, :])
   if i > 200:
      break
"""

