import numpy as np
import pandas as pd

from matplotlib import pyplot as plt

from sklearn.preprocessing import LabelEncoder, StandardScaler
from sklearn.cluster import KMeans

from plot_decision_boundaries import plot_decision_boundaries

coe_df = pd.read_csv('data/coe-results.csv', usecols=['vehicle_class', 'quota', 'premium'])

print(coe_df.columns)

quota_premium_df = coe_df.loc[:, ['quota', 'premium']]

vehicle_class_df = coe_df.loc[:, ['vehicle_class']]

#encode the vehicle_class
le = LabelEncoder()
le.fit(vehicle_class_df)

vehicle_encoded_class_df = pd.DataFrame(le.transform(vehicle_class_df))

#scale quote and premium

sc = StandardScaler()
sc.fit(quota_premium_df)

quota_premium_scaled_df = sc.transform(quota_premium_df)

print('Labels: \n', vehicle_encoded_class_df[:5])
print('Data: \n', quota_premium_scaled_df[:5])

n_classes = len(coe_df.loc[:, 'vehicle_class'].unique())
n_iterations = [1, 5, 10, 20]


fig = plt.figure()
sp = []
sp_count = 220

for x in range(len(n_iterations)):
   sp_count += 1
   print('subplot = ', sp_count)

   print('Iterations: %d' %x)
   kmeans = KMeans(init='random', n_clusters=n_classes, max_iter=n_iterations[x], n_init=1)
   kmeans.fit(quota_premium_scaled_df)
   sp.append(fig.add_subplot(sp_count))

   plot_decision_boundaries(n_iterations[x], sp[x], kmeans, quota_premium_scaled_df)

plt.show()
