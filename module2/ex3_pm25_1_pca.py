import sys
import numpy as np
import pandas as pd

from sklearn.decomposition import PCA

from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import axes3d

csv_file = 'data/processed_PRSA_data_2010.1.1-2014.12.31.csv' \
      if len(sys.argv) <= 1 else sys.argv[1]

pm25_pd = pd.read_csv(csv_file, index_col=0 \
      , infer_datetime_format=True, parse_dates=['date_time'])

y_pm25 = pm25_pd.loc[:, 'pm2.5']
X_pm25 = pm25_pd.loc[:, ~pm25_pd.columns.isin(['date_time', 'pm2.5', 'No', 'cbwd'])]

pca = PCA(n_components=2)
x_y = pca.fit_transform(X_pm25)

fig = plt.figure()

ax = fig.add_subplot(111, projection='3d')
ax.scatter(x_y[:, 0], x_y[:, 1], y_pm25)

plt.show()
