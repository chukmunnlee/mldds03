import pickle
import numpy as np
import pandas as pd

from sklearn.preprocessing import LabelEncoder

import spacy
from spacy.lemmatizer import Lemmatizer
from spacy.lang.en import LEMMA_INDEX, LEMMA_EXC, LEMMA_RULES

news_df = pd.read_csv('data/Full-Economic-News-DFE-839861.csv',  encoding='latin-1')
news_df = news_df.loc[news_df.relevance != 'not sure', :]

relevance_df = news_df.loc[:, ['relevance']]
relevance_df.columns = ['relevance']

headline_df = news_df.loc[:, ['headline']]
headline_df.columns = [ 'headline' ]

encoder_relevance = LabelEncoder()

encoder_relevance.fit(relevance_df.values.ravel())
relevance_encoded = encoder_relevance.transform(relevance_df.values.ravel())

def tokenize(text):
   doc = nlp(text)
   lemmas = [ lemmatizer(tok.text, tok.pos_) for tok in doc \
         if not tok.is_stop and tok.is_alpha ]
   return ' '.join([ w for stmt in lemmas for w in stmt ])

nlp = spacy.load('en_core_web_sm')
lemmatizer = Lemmatizer(LEMMA_INDEX, LEMMA_EXC, LEMMA_RULES)

print('Start tokenizing...')
headline_tokens_df = headline_df.headline.apply(tokenize)

pickle.dump((relevance_encoded, headline_tokens_df, encoder_relevance), \
      open('data/news0.pickle', 'wb'))
