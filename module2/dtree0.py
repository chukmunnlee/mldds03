import numpy as np
import pandas as pd
import graphviz

from sklearn.preprocessing import LabelEncoder 
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier, export_graphviz


cols = ['sector', 'type_of_expenditure', 'type_of_cost', 'rnd_expenditure']

df = pd.read_csv('data/research-and-development-expenditure-by-type-of-cost.csv', usecols=cols)

le = LabelEncoder()

for c in ['sector', 'type_of_expenditure', 'type_of_cost']:
   df[ c + '_c' ] = le.fit_transform(df[c])

data = df[['rnd_expenditure', 'type_of_expenditure_c', 'type_of_cost_c']]
target = df['sector_c']

X_train, X_test, y_train, y_test = train_test_split(data, target)

dtree = DecisionTreeClassifier(random_state=42)
dtree.fit(X_train, y_train)

result = dtree.predict(X_test)
prob_result = dtree.predict_proba(X_test)

#print(result)
#print(prob_result)

le = LabelEncoder()
le.fit(df['sector'])
feature_names = ['rnd_expenditure', 'type_of_expenditure', 'type_of_cost']
target_names = le.classes_
filename = 'data/research.dot'

export_graphviz(dtree, feature_names=feature_names, class_names=target_names, \
      out_file=filename, filled=True, rounded=True)

source = graphviz.Source.from_file(filename)
source.render(view=True)

