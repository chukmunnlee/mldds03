import pickle
import numpy as np
import pandas as pd

from matplotlib import pyplot as plt

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.manifold import TSNE
from sklearn.model_selection import train_test_split

relevance_encoded, headline_tokens_df, encoder_relevance = pickle.load( \
      open('data/news0.pickle', 'rb'))

relevance_encoded_df = pd.DataFrame(relevance_encoded)
relevance_encoded_df.columns = [ 'relevance' ]

X_headline_train_df, X_headline_test_df, y_relevance_train_df, y_relevance_test_df = \
      train_test_split(headline_tokens_df, relevance_encoded_df)

tfidf_vectorizer = TfidfVectorizer(lowercase=False, decode_error='ignore')

tfidf_vectorizer.fit(X_headline_train_df)

#headline_vectorized = tfidf_vectorizer.transform(headline_tokens_df)
#headline_vectorized_dense = headline_vectorized.todense()

pickle.dump((X_headline_train_df, y_relevance_train_df, X_headline_test_df, y_relevance_test_df, tfidf_vectorizer), \
      open('data/news1.pickle', 'wb'))

"""
headline_vectorized_dense_df = pd.DataFrame(headline_vectorized_dense)

headline_sample = headline_vectorized_dense_df.sample(frac=0.2)

tsne = TSNE(n_components=2, random_state=42)
print('Fitting...')
X_2d = tsne.fit_transform(headline_sample)
print('Fitting done...')

fig = plt.figure()

ax = fig.add_subplot(111)
ax.scatter(X_2d[:, 0], X_2d[:, 1], c=relevance_encoded_df.relevance)

plt.plot()
"""
