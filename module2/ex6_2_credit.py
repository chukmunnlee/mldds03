import numpy as np
import pandas as pd
import pickle

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d

from sklearn.decomposition import PCA
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LogisticRegression, SGDClassifier
from sklearn.metrics import classification_report

crx_onehot_df, y_df = pickle.load(open('data/crx_onehot.pickle', 'rb'))

X_train, X_test, y_train, y_test = train_test_split(crx_onehot_df, y_df)

print('train: ', len(X_train), len(y_train))
print('test: ', len(X_test), len(y_test))

#do not need to scale Y because its binary 1 or 0
scaler = StandardScaler()
scaler.fit(X_train)
X_train_scaled_df = pd.DataFrame(scaler.transform(X_train))

lr = LogisticRegression(random_state=42)
lr.fit(X_train, y_train)

result = lr.predict(X_test)

print('logistic mean accuracy: ', lr.score(X_test, y_test))

print(classification_report(y_test, result))

sgdlr = SGDClassifier(max_iter=1000, verbose=False)
sgdlr.fit(X_train, y_train)

result = lr.predict(X_test)

print('sgd logistic mean accuracy: ', lr.score(X_test, y_test))

print(classification_report(y_test, result))

