import numpy as np
import pandas as pd
import pickle

from sklearn.preprocessing import LabelEncoder
from sklearn.feature_extraction.text import TfidfVectorizer

from matplotlib import pyplot as plt

import spacy
from spacy.lemmatizer import Lemmatizer
from spacy.lang.en import LEMMA_INDEX, LEMMA_EXC, LEMMA_RULES

nlp = spacy.load('en')
lemmatizer = Lemmatizer(LEMMA_INDEX, LEMMA_EXC, LEMMA_RULES)

def tokenize_text(text):
   doc = nlp(text)
   lemmas = [ lemmatizer(token.text, token.pos_) for token in doc \
         if not token.is_stop or token.is_alpha ]
   return ' '.join([item for sublist in lemmas for item in sublist])

jobs_df = pd.read_csv('data/job_skills.csv')

jobs_df.isnull().values.any()

jobs_df.dropna(inplace=True)

X_responsibilities_df = jobs_df.loc[:, [ 'Responsibilities' ]]
Y_title_df = jobs_df.loc[:, [ 'Title' ]]
Y_category_df = jobs_df.loc[:, [ 'Category' ]]

#print(Y_category_df.groupby('Category').size())

#loc - values of the 
Y_category_df.loc[jobs_df.Category == 'Data Center & Network', 'Category'] = 'IT & Data Management'
Y_category_df.loc[jobs_df.Category == 'Network Engineering', 'Category'] = 'IT & Data Management'

le = LabelEncoder()
Y_category_encoded_df = pd.DataFrame(le.fit_transform(Y_category_df.Category))
Y_category_encoded_df.columns = [ 'Category' ]

#print(Y_category_encoded_df.loc[Y_category_encoded_df.Category == 1])

print('before processing')
tokenized_df = X_responsibilities_df.Responsibilities.apply(tokenize_text)
print('after processing')

print('len = ', len(tokenized_df), len(Y_category_encoded_df))

pickle.dump((tokenized_df, Y_category_encoded_df), open('data/tokenized_df.pickle', 'wb'))

#print('original: ', X_responsibilities_df.Responsibilities[0])
#print('original len: ', len(X_responsibilities_df.Responsibilities[0]))
#print('tokenized: ', tokenize_text(X_responsibilities_df.Responsibilities[0]))
#print('tokenized len: ', len(tokenize_text(X_responsibilities_df.Responsibilities[0])))

