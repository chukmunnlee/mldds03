import numpy as np
import pandas as pd
import pickle

from sklearn.model_selection import train_test_split, learning_curve
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LogisticRegression, SGDClassifier
from sklearn.metrics import classification_report

from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

columns = np.array([ ['S%d' %d, 'C%d' %d ] for d in range(1, 6) ]).flatten()
columns = np.append(columns, ['poker'])

df = pd.read_csv('data/poker-hand-training-true.data', names=columns)
#df = pd.read_csv('data/poker-hand-testing.data', names=columns)
shuffled_df = df.sample(frac=1)

poker_hand_df = shuffled_df.loc[:, 'poker']
cards_df = shuffled_df.loc[:, ~shuffled_df.columns.isin(['poker'])]

new_df = pd.DataFrame()

for i in range(1, 6):
   s = 'S%d' %i
   c = 'C%d' %i
   suit = cards_df.filter([s], axis=1)
   encoded_df = pd.get_dummies(suit, columns=[s])
   new_df = pd.concat([new_df, encoded_df, cards_df.filter([c], axis=1)], axis=1)

card_df = new_df

print(new_df.columns)

lr = LogisticRegression(random_state=42)
lr.fit(cards_df, poker_hand_df)
print('> intercept ', lr.intercept_)
print('> coefficient ', lr.coef_)

print(poker_hand_df.shape)

#pickle.dump(lr, open('data/poker_lr.pickle', 'wb'))
#
#result = lr.predict(cards_df)
#
#print('TRAIN RESULT')
#print(classification_report(poker_hand_df, result))

#sgdc = SGDClassifier(max_iter=1000, verbose=True)
#sgdc.fit(cards_df, poker_hand_df)

#estimator = lr
#
#train_size, train_score, validation_score = learning_curve(estimator, cards_df, poker_hand_df \
#      , train_sizes=np.arange(.1, 1.1, .1))
#
#train_mean = np.mean(train_score, axis=1)
#validation_mean = np.mean(validation_score, axis=1)
#
#print('train mean = ', train_mean.shape)
#print('validation mean = ', validation_mean.shape)
#
#plt.plot(train_size, train_mean, label='Training', color='b', marker='o')
#plt.plot(train_size, validation_mean, label='Validation', color='g', marker='x')
#
#plt.legend()
#
#plt.show()


#predict with test data
print('TEST RESULT')
df_real = pd.read_csv('data/poker-hand-testing.data', names=columns)
#df_real = pd.read_csv('data/poker-hand-training-true.data', names=columns)
shuffled_df = df_real.sample(frac=1)

test_poker_hand_df = shuffled_df.loc[:, 'poker']
test_cards_df = shuffled_df.loc[:, ~shuffled_df.columns.isin(['poker'])]

result = lr.predict(test_cards_df)

print(classification_report(test_poker_hand_df, result))

##plot PCA
#pca = PCA(n_components=2)
#x_y = pca.fit_transform(cards_df)
#
#fig = plt.figure()
#ax = fig.add_subplot(111, projection='3d')
#ax.scatter(x_y[:, 0], x_y[:, 1], poker_hand_df, s=1)
##ax.scatter(x_y[poker_hand_df==0, 0], x_y[poker_hand_df==0, 1], x_y[poker_hand_df==0, 2], color='r')
#
#plt.show()


