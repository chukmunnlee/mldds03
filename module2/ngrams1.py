import pickle
import numpy as np
import pandas as pd

from sklearn.feature_extraction.text import TfidfVectorizer

category_encoded_df, responsibilties_tokenized_df, category_encoder = \
      pickle.load(open('data/ngram0.pickle', 'rb'))

vectorizer = TfidfVectorizer(ngram_range=(2, 2), decode_error='ignore')
vectorizer.fit(responsibilties_tokenized_df)

print('Vectorizing ngram=(2, 2)')
result = vectorizer.transform(responsibilties_tokenized_df.values.ravel()).todense()
print('Done')

pickle.dump((category_encoded_df, result, category_encoder, vectorizer), \
      open('data/ngram1.pickle', 'wb'))

print(result)
print(vectorizer.get_feature_names())
print(vectorizer.vocabulary_)
print(vectorizer.idf_)
