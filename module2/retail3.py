import numpy as np
import pandas as pd

from matplotlib import pyplot as plt

from sklearn.cluster import KMeans
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from sklearn.model_selection import train_test_split

from plot_decision_boundaries import plot_decision_boundaries

CLUSTERS = 5
ITERATIONS = 20

online_df = pd.read_csv('data/online_combined.csv')

cluster_columns = [ 'StockCode', 'Quantity', 'UnitPrice', 'Country', ]
selected_df = online_df.loc[:, cluster_columns]

scaler = StandardScaler()
selected_scaled_df = scaler.fit_transform(selected_df)

#kmeans = KMeans(n_clusters=CLUSTERS)
#kmeans.fit(selected_scaled_df)

pca = PCA(n_components=2)
X_2d = pca.fit_transform(selected_scaled_df)

kmeans = KMeans(n_clusters=CLUSTERS)
kmeans.fit(X_2d)

#centroids_2d = pca.transform(kmeans.cluster_centers_)
centroids_2d = kmeans.cluster_centers_

fig = plt.figure()
ax = fig.add_subplot(111)

plot_decision_boundaries(-1, ax, kmeans, X_2d)

ax.scatter(centroids_2d[:, 0], centroids_2d[:, 1], marker='x')

plt.show()

