import numpy as np
import pandas as pd

columns = np.array([ ['S%d' %d, 'C%d' %d ] for d in range(1, 6) ]).flatten()
columns = np.append(columns, ['poker'])

df = pd.read_csv('data/poker-hand-testing.data', names=columns)
shuffled_df = df.sample(frac=1)

new_df = pd.DataFrame()

poker_hand_df = shuffled_df.loc[:, 'poker']
cards_df = shuffled_df.loc[:, ~shuffled_df.columns.isin(['poker'])]

print(pd.get_dummies(cards_df, columns=['S1', 'S2']).columns)

for i in range(1, 6):
   s = 'S%d' %i
   c = 'C%d' %i

   suit = cards_df.filter([s], axis=1)
   encoded_df = pd.get_dummies(suit, columns=[s])
   new_df = pd.concat([new_df, encoded_df, cards_df.filter([c], axis=1)], axis=1)
