import sys
import numpy as np
import pandas as pd
import pickle

from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import SGDRegressor
from sklearn.metrics import mean_squared_error, mean_squared_log_error, r2_score

pickle_file = 'data/sgd_regressor-2000.pickle' \
      if len(sys.argv) <= 1 else sys.argv[1]

csv_file = 'data/processed_PRSA_data_2010.1.1-2014.12.31.csv' \
      if len(sys.argv) <= 2 else sys.argv[2]


X_test, y_test, x_scaler, y_scaler, sgdr = pickle.load(open(pickle_file, 'rb'))

result = sgdr.predict(X_test)

print('Test: MSE = ', mean_squared_error(y_test, result))
print('Test: R2 score = ', r2_score(y_test, result))

