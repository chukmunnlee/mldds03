import numpy as np
import pandas as pd

from sklearn.feature_extraction.text import TfidfVectorizer

text = [ "I like to eat", "Sleeping is fun" ]

tfidf_vectorizer = TfidfVectorizer()
tfidf_vectorizer.fit(text)
vectorized = tfidf_vectorizer.transform(text).todense()

#get the tfidf
print(vectorized)

print(tfidf_vectorizer.get_feature_names())
print(tfidf_vectorizer.vocabulary_)
print(tfidf_vectorizer.idf_)
print(tfidf_vectorizer.stop_words_)

#from spacy.lang.en.stop_words import STOP_WORDS

#news_df = df.read_csv('data/XXX.csv')
