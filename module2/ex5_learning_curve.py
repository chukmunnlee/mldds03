import sys
import numpy as np
import pandas as pd
import pickle

from sklearn.model_selection import train_test_split, learning_curve
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import SGDRegressor, LinearRegression
from sklearn.metrics import mean_squared_error, mean_squared_log_error, r2_score

from matplotlib import pyplot as plt

SGD_ITERATION = 2000
csv_file = 'data/processed_PRSA_data_2010.1.1-2014.12.31.csv' \
      if len(sys.argv) <= 1 else sys.argv[1]

#read file
pm25_df = pd.read_csv(csv_file, index_col=0 \
      , infer_datetime_format=True, parse_dates=['date_time'])

#isolate pm2.5 column to a dataframe
y_pm25 = pm25_df.loc[ :, 'pm2.5']
#everything else to another dataframe
X_pm25 = pm25_df.loc[ :, ~pm25_df.columns.isin(['pm2.5', 'date_time'])]

#split data
X_train, X_test, y_train, y_test = train_test_split(X_pm25, y_pm25)

print('train: x=%d, y=%d' %(len(X_train), len(y_train)))
print('test: x=%d, y=%d' %(len(X_test), len(y_test)))

scaler_x = StandardScaler()
scaler_y = StandardScaler()

scaler_x.fit(X_train)
X_scaled_train = pd.DataFrame(scaler_x.transform(X_train))

scaler_y.fit(y_train[:, None])
y_scaled_train = pd.DataFrame(scaler_y.transform(y_train[:, None]))

X_scaled_test = pd.DataFrame(scaler_x.transform(X_test))
y_scaled_test = pd.DataFrame(scaler_y.transform(y_test[:, None]))

print('train scaled: x=%d, y=%d' %(len(X_scaled_train), len(y_scaled_train)))
print('test scaled: x=%d, y=%d' %(len(X_scaled_test), len(y_scaled_test)))

#sgdr = SGDRegressor(n_iter=SGD_ITERATION, verbose=True)
lr = LinearRegression()
sgdr = SGDRegressor()

#train_size, train_score, test_score = learning_curve(sgdr, X_scaled_train, y_scaled_train)
#train_size, train_score, validation_score = learning_curve(lr, X_scaled_train, y_scaled_train)
#cv=defaults to 3 fold, cv=10 10-fold, in general K-fold
#divide the data/train into K segments
train_size, train_score, validation_score = learning_curve(sgdr, X_scaled_train, y_scaled_train \
      , train_sizes=np.arange(.1, 1.1, .1))

print('LR: train_size = ', train_size)
print('LR: train_score = ', train_score)
print('LR: test_score = ', validation_score)
print('LR: len test_score = ', len(validation_score))

train_mean = np.mean(train_score, axis=1)
validation_mean = np.mean(validation_score, axis=1)

print('shape = ', validation_mean.shape)
print('columns = ', X_train.columns)

for s, tm, vm in zip(train_size, train_mean, validation_mean):
   print(s, ', ', tm, ', ', vm)

plt.plot(train_size, train_mean, label='Training', color='blue', marker='o')
plt.plot(train_size, validation_mean, label='Validation', color='orange', marker='o')

plt.legend()

plt.show()

#plt.plot(X, train_score, label='Train', color='green')
#plt.plot(X, validation_score, label='Validate', color='blue')
#
#plt.show()
