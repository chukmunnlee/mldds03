import numpy as np
import pandas as pd

from mpl_toolkits.mplot3d import Axes3D

from matplotlib import pyplot as plt

from sklearn.decomposition import PCA

online_pd = pd.read_csv('data/online_combined.csv')

pca = PCA(n_components=3)

print('Start PCA')
X_3d = pca.fit_transform(online_pd)
print('\t done')

fig = plt.figure()

ax = fig.add_subplot(111, projection='3d')

ax.scatter(X_3d[:, 0], X_3d[:, 1], X_3d[:, 2], s=3, cmap=plt.cm.Pastel2)

plt.show()
