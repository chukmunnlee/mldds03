import numpy as np
from matplotlib import pyplot as plt

p = np.linspace(0.01, 0.99, 100)
y_1 = np.ones(p.shape)
y_0 = np.zeros(p.shape)

def log_loss(y, p):
   return -(y * np.log(p) + (1 - y) * np.log(1 - p))

plt.plot(p, log_loss(y_1, p), label='Log loss for y=1')
plt.plot(p, log_loss(y_0, p), label='Log loss for y=0')

plt.legend()

plt.show()
