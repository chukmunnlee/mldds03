import pickle
import numpy as np
import pandas as pd

from sklearn.linear_model import LogisticRegression, SGDClassifier

category_encoded_df, responsibilties_tokenized_df, _ = pickle.load( \
      open('data/jobs_stage0.pickle', 'rb'))

responsibilties_vectorized, tfidf_vectorizer = pickle.load(open('data/jobs_stage1.pickle', 'rb'))
responsibilties_vectorized_dense = responsibilties_vectorized.todense()

category_encoded_reshaped = category_encoded_df.values.reshape(len(category_encoded_df))

print('Fitting logistic regression')
lr = LogisticRegression()
lr.fit(responsibilties_vectorized_dense, category_encoded_reshaped)

print('Fitting sgd regressor')
sgdclassifier = SGDClassifier(max_iter=1000, random_state=42)
sgdclassifier.fit(responsibilties_vectorized_dense, category_encoded_reshaped)

pickle.dump((category_encoded_df, tfidf_vectorizer, lr, sgdclassifier), \
      open('data/jobs_stage2.pickle', 'wb'))
