import numpy as np
import pandas as pd
import pickle

from sklearn.preprocessing import LabelEncoder 

import spacy
from spacy.lemmatizer import Lemmatizer
from spacy.lang.en import LEMMA_INDEX, LEMMA_EXC, LEMMA_RULES

jobs_df = pd.read_csv('data/job_skills.csv')

jobs_df.dropna(inplace=True)

category_df = pd.DataFrame(jobs_df.loc[:, 'Category' ], columns = [ 'Category' ])
responsibilties_df = pd.DataFrame(jobs_df.loc[:, 'Responsibilities' ], columns = [ 'Responsibilities' ])

category_df.loc[category_df.Category == 'Data Center & Network', 'Category' ] = \
      'IT & Data Management'
category_df.loc[category_df.Category == 'Network Engineering', 'Category' ] = \
      'IT & Data Management'

#encode the labels
category_encoder = LabelEncoder()
#values: convert pandas to numpy
#ravel (X, 1) => (X,)
#cat = category_df.values.ravel()
#category_encoder.fit(cat)
category_encoder.fit(category_df.Category)

category_encoded_df = pd.DataFrame(category_encoder.transform(category_df.Category), columns = [ 'Category' ])

#tokenize the text
def tokenize(text):
   doc = nlp(text)
   lemmas = [ lemmatizer(token.text, token.pos_) for token in doc \
         if not token.is_stop and token.is_alpha ]
   return ' '.join([ word for sentence in lemmas for word in sentence ])

nlp = spacy.load('en_core_web_sm')
lemmatizer = Lemmatizer(LEMMA_INDEX, LEMMA_EXC, LEMMA_RULES)

print('Start tokenizing....')
responsibilties_tokenized_df = responsibilties_df.Responsibilities.apply(tokenize)

pickle.dump((category_encoded_df, responsibilties_tokenized_df, category_encoder), \
      open('data/jobs_stage0.pickle', 'wb'))
