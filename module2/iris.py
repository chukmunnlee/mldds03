import numpy as np
import pandas as pd

from matplotlib import pyplot as plt

from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
from sklearn.naive_bayes import GaussianNB
from sklearn import linear_model, datasets

iris = datasets.load_iris()

X = pd.DataFrame(data=iris.data)
X.columns = ['F%d'%i for i in  range(0, 4) ]
y = pd.DataFrame(iris.target)
y.columns = [ 'name' ]

print('sample size = ', len(X))

scaler_X = StandardScaler()
scaler_X.fit(X)
X_scaled = scaler_X.transform(X)

gaussian = GaussianNB()

pca = PCA(n_components=2)

X_scaled_2d = pca.fit_transform(X_scaled)

gaussian.fit(X_scaled_2d, y.name)

X0, X1 = X_scaled_2d[:, 0], X_scaled_2d[:, 1]
X0_min, X0_max = X0.min() - 1, X0.max() + 1
X1_min, X1_max = X1.min() - 1, X1.max() + 1

xx, yy = np.meshgrid(np.arange(X0_min, X0_max), np.arange(X1_min, X1_max))
Z = gaussian.predict(np.c_[xx.ravel(), yy.ravel()])
print(xx.shape)
print(Z.reshape(xx.shape))

fig = plt.figure()
ax = fig.add_subplot(111)
ax.contourf(xx, yy, Z.reshape(xx.shape), cmap=plt.cm.coolwarm, alpha=0.8)

ax.set_xlim(xx.min(), xx.max())
ax.set_ylim(yy.min(), yy.max())

ax.scatter(X0, X1, c=y.name, cmap=plt.cm.coolwarm, s=20, edgecolors='k')

ax.set_title('Iris plot samples=%d' %len(X_scaled))

plt.show()
