import numpy as np
import pandas as pd

from sklearn.preprocessing import LabelEncoder
from sklearn.feature_extraction.text import TfidfVectorizer

import spacy
from spacy.lemmatizer import Lemmatizer
from spacy.lang.en import LEMMA_INDEX, LEMMA_EXC, LEMMA_RULES

SAMPLE_SIZE=10000

['InvoiceNo', 'StockCode', 'Description', 'Quantity', 'InvoiceDate', 'UnitPrice', 'CustomerID', 'Country']

def tokenize(text):
   lemmas = [ lemmatizer(tok.text, tok.pos_) for tok in nlp(text) \
         if not tok.is_stop and tok.is_alpha ]
   return ' '.join([ w for stmt in lemmas for w in stmt ])

online_df = pd.read_csv('data/online_retail.csv')

online_df.dropna(inplace=True)

#online_sample_df = online_df.sample(frac=.3)
online_sample_df = online_df.sample(n=SAMPLE_SIZE, random_state=42)

print('sample = ', len(online_sample_df))

for col in [ 'InvoiceNo', 'StockCode', 'Country' ]:
   encoder = LabelEncoder()
   online_sample_df[col] = encoder.fit_transform(online_sample_df[col])

nlp = spacy.load('en_core_web_sm')
lemmatizer = Lemmatizer(LEMMA_INDEX, LEMMA_EXC, LEMMA_RULES)

description_tok = online_sample_df.Description.apply(tokenize)

vectorizer = TfidfVectorizer(lowercase=True, decode_error='ignore')
description_vec = vectorizer.fit_transform(description_tok)

description_df = pd.DataFrame(description_vec.toarray(), \
      columns=vectorizer.get_feature_names(), \
      index=online_sample_df.index)

print('features: ', vectorizer.get_feature_names()[:100])

online_combined_df = pd.concat([online_sample_df, description_df], axis=1)

to_drop = ['Description', 'InvoiceDate' ]
online_combined_df.drop(labels=to_drop, axis=1, inplace=True)

online_combined_df.to_csv('data/online_combined.csv')

#one_hot_columns = [ 'StockCode', 'Country' ]
#online_onehot_encoded = pd.get_dummies(online_df, columns=one_hot_columns)

