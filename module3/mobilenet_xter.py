import os, sys, time

import requests 
import numpy as np

from PIL import Image, ImageOps

from keras.optimizers import Adam
from keras.models import Sequential, model_from_json
from keras.layers import Dense, Dropout, Flatten
#helper functions provided by various models
from keras.applications.mobilenet import MobileNet, preprocess_input
from keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import TensorBoard, EarlyStopping

#classifier (frozen) + classifier (new)

img_width, img_height = 160, 160
channels = 3

#Drop the classifier part the last dense layer, include_top=False
#include_top=False  conv_pw_13_relu (ReLU) - drops the classifier
#       (None, 5, 5, 1024)        0
#include_top=True - includes the classifier outputs a prediction as a 1-hot vector
#       reshape_2 (Reshape)          (None, 1000)              0  
featurizer = MobileNet(input_shape=(img_height, img_width, channels), \
      include_top=False)

featurizer.summary()

gen = ImageDataGenerator(rotation_range=45.0, horizontal_flip=True, \
      zoom_range=3.) # pixels 0 - 255 -> 0 - 1 range
      #zoom_range=3., rescale=1./255) # pixels 0 - 255 -> 0 - 1 range

it = gen.flow_from_directory('data/food/train', target_size=(img_height, img_width), \
      batch_size=45) #15 images x 3 classes

#y_train is one hot labels
X_train, y_train = next(it)

X_train = preprocess_input(X_train)

print('X_train.shape = ', X_train.shape)
print('y_train.shape = ', y_train.shape)
print('classes = ', np.argmax(y_train, axis=1))

"""
Transform X_train to features
1. images (45), labels (45) => [featurizer] => features (45 rows, 5x5x1024 features)
2. fit classifier using features and labels(y_train)
3. predict
"""

#Step 1
X_train_features = featurizer.predict(X_train)
print('X_train.shape = ', X_train.shape)
print('X_train_features.shape = ', X_train_features.shape)
print('X_train_features.shape = ', X_train_features.shape[1:])


#Step 2
classifier = Sequential()
##error here - input_shape !!! check !!!
classifier.add(Flatten(input_shape=X_train_features.shape[1:])) # 5, 5, 1024 => vector (5x5x1024)
classifier.add(Dense(64, activation='relu')) #first FC layer
classifier.add(Dropout(0.5)) # to prevent overfitting
#3 or more clases: softmax - multiclass
#2 classes sigmoid
classifier.add(Dense(3, activation='softmax'))

classifier.summary()

#Step 3
#loss function see https://keras.io/losses/#available-loss-functions
#loss = binary classification: binary_crossentropy
#loss = multiclass classification: categorical_crossentropy
#loss = regression: mean_squared_error
classifier.compile(optimizer=Adam(), loss="categorical_crossentropy", \
      metrics=['accuracy'])

tensorboard = TensorBoard(log_dir='logs/classifier/%s' %int(time.time()), \
      histogram_freq=0, write_graph=True, write_images=False)

earlystop = EarlyStopping(monitor='val_loss', patience=8)

#train 
classifier.fit(X_train_features, y_train, callbacks=[tensorboard, earlystop], \
      validation_split=0.2, epochs=50)


#predict
print('##### Validation #####')

#test_gen = ImageDataGenerator(rescale=1./255)
test_gen = ImageDataGenerator()
it = test_gen.flow_from_directory('data/food/validation', \
      target_size=(img_height, img_width), batch_size=15) #number of images in validation set

X_test, y_test = next(it)

X_test = preprocess_input(X_test)

X_test_features = featurizer.predict(X_test)

##save all
print('Saving Featurizer...')
with open('data/mobnet_featurizer.json', 'w') as json_file:
   json_file.write(featurizer.to_json())
featurizer.save_weights('data/mobnet_featurizer.h5')

print('Saving Classifier...')
with open('data/food_classifier.json', 'w') as json_file:
   json_file.write(classifier.to_json())
classifier.save_weights('data/food_classifier.h5')

print('true\t\t', np.argmax(y_test, axis=1))

y_predict = classifier.predict(X_test_features)
print('predicted\t', np.argmax(y_predict, axis=1))

y_predict = classifier.predict_classes(X_test_features)
print('predicted\t', y_predict)

url = 'https://thumbs.dreamstime.com/b/indian-chapati-bread-isolated-white-background-33601745.jpg' \
      if len(sys.argv) <= 1 else sys.argv[1]

r = requests.get(url, stream=True)
i_img = Image.open(r.raw)
i_img = ImageOps.fit(i_img, (img_width, img_height))
i_img = np.expand_dims(i_img, axis=0)
i_img = preprocess_input(i_img)


print('shape = ', i_img.shape)

X_i_img = featurizer.predict(i_img)

y_i_labels = classifier.predict_classes(X_i_img)
y_i_prob = classifier.predict(X_i_img)

labels = [ 'chapati', 'fishball_noodle', 'satay' ]

print('predicted class: %s, probability: %.03f' %(labels[y_i_labels[0]], y_i_prob[0, y_i_labels[0]]))

