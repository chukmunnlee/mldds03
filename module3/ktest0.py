import numpy as np

from PIL import Image
from cairosvg import svg2png

from matplotlib import pyplot as plt
from cairosvg import svg2png

from keras.models import Sequential
from keras.layers import Dense
from keras.utils.vis_utils import model_to_dot, plot_model

simple_model = Sequential()
simple_model.add(Dense(1, input_dim=4, activation='sigmoid'))
print('------- simple ----------')
simple_model.summary()

deep_model = Sequential()
#16 + 1 to 256 nodes, activation is relu
deep_model.add(Dense(256, input_dim=16, activation='relu'))
#256 + 1 to 1 node, activation is sigmoid
deep_model.add(Dense(1, activation='sigmoid'))
print('------- 2 layers ----------')
deep_model.summary()

plot_model(deep_model, show_shapes=True, show_layer_names=True, to_file='data/deep_model2.png')
#layer_svg = model_to_dot(deep_model, show_shapes=True).create(prog='dot', format='svg')
#layer_png = model_to_dot(deep_model, show_shapes=True).plot_model
#print(layer_png)
#svg2png(bytestring=layer_svg, write_to='data/deep_model.png')

img = np.array(Image.open(open('data/deep_model2.png', 'rb')))
#
fig = plt.figure()
ax = fig.add_subplot(111)
ax.imshow(img)
plt.show()
