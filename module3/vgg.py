import numpy as np

from matplotlib import pyplot as plt

from PIL import Image, ImageOps

from keras.preprocessing.image import img_to_array

from keras.applications.vgg16 import VGG16, preprocess_input, decode_predictions

def resize_and_crop(img_path, w, h):
   img = Image.open(img_path)
   return ImageOps.fit(img, (w, h))

path = 'data/DOGS8.jpg'
path = 'data/Bat-Dog.png'
path = 'data/dog.jpg'
path = 'data/hobbes.jpg'
path = 'data/fbd.jpg'
path = 'data/f20.png'

vgg16 = VGG16()

#print(vgg16.summary())

img = resize_and_crop(path, 224, 224)
img_arr = img_to_array(img)
img_pre = preprocess_input(img_arr)

#VGG16 expects another dimension in front of the image
#(224, 224, 3) -> (1, 224, 224, 3)
#img_pre = np.expand_dims(img_pre, axis=0)
img_pre = img_pre[None, :, :, :]

print(img_pre.shape)

raw_pred = vgg16.predict(img_pre)

decode_pred = decode_predictions(raw_pred, top=5)

print('Raw predictions\n', raw_pred)

print('file: ', path)
print('Decoded predictions\n', decode_pred)



#plt.imshow(img_arr)

#plt.show()
