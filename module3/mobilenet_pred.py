import os, sys

import requests

import numpy as np

from PIL import Image, ImageOps

from keras.models import model_from_json

from keras.applications.mobilenet import preprocess_input

img_width, img_height = 160, 160

print('Loading Featurizer...')
with open('data/mobnet_featurizer.json', 'r') as json_file:
   featurizer = model_from_json(json_file.read())
featurizer.load_weights('data/mobnet_featurizer.h5')

print('Loading Classifier...')
with open('data/food_classifier.json', 'r') as json_file:
   classifier = model_from_json(json_file.read())
classifier.load_weights('data/food_classifier.h5')

print('----- featurizer -----')
featurizer.summary()

print('----- classifier -----')
classifier.summary()

labels = [ 'chapati', 'fishball_noodle', 'satay' ]

url = 'https://thumbs.dreamstime.com/b/indian-chapati-bread-isolated-white-background-33601745.jpg' \
      if len(sys.argv) <= 1 else sys.argv[1]

r = requests.get(url, stream=True)

img = Image.open(r.raw)
img = ImageOps.fit(img, (img_width, img_height))
img_arr = np.expand_dims(img, axis=0)
img_arr = preprocess_input(img_arr)

X_to_predict = featurizer.predict(img_arr)
y_label = classifier.predict_classes(X_to_predict)
y_prob = classifier.predict(X_to_predict)

print('\nurl: %s\n' %url)
print('class: %s, probability: %.3f' %(labels[y_label[0]], y_prob[0, y_label[0]]))
