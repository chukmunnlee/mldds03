import numpy as np
from PIL import Image
from matplotlib import pyplot as plt

dog = np.array(Image.open(open('data/dog.jpg', 'rb')))

fig = plt.figure()
x = fig.add_subplot(421)
x.imshow(dog)

r = fig.add_subplot(423)
r.imshow(dog[..., 0])
rh = fig.add_subplot(424)
rh.hist(dog[..., 0].flatten(), 256, range=(0, 256), color='red')

g = fig.add_subplot(425)
g.imshow(dog[..., 1])
gh = fig.add_subplot(426)
gh.hist(dog[..., 1].flatten(), 256, range=(0, 256), color='green')

b = fig.add_subplot(427)
b.imshow(dog[..., 2])
bh = fig.add_subplot(428)
bh.hist(dog[..., 2].flatten(), 256, range=(0, 256), color='blue')


plt.show()
