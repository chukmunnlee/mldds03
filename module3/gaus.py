import numpy as np

from scipy.stats import norm 

from matplotlib import pyplot as plt 

def plot_gaussian(x, mean, sigma):
   z = norm.pdf(x, loc=mean, scale=sigma)
   print(z)
   plt.plot(x, z)

x = np.arange(-5, 5, .01)

plot_gaussian(x, 0, 1)
plot_gaussian(x, -2, 1)
plot_gaussian(x, 2, 1)
plot_gaussian(x, 0, 1.5)

plt.grid()
plt.show()
