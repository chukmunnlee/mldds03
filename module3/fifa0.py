import pickle
import numpy as np
import pandas as pd

from sklearn.preprocessing import LabelEncoder

fifa_df = pd.read_csv('data/FIFA_2018_Statistics.csv', \
      parse_dates=['Date'], infer_datetime_format=['Date'])

#fill all the NaN with 0.0
val = fifa_df.loc[:, ['Own goals', 'Own goal Time']].fillna(value=0, axis=1)
fifa_df.loc[:, ['Own goals', 'Own goal Time']] = val

#encode 
yn_encoder = LabelEncoder()
moth_encoded = yn_encoder.fit_transform(fifa_df.loc[:, ['Man of the Match']].values.ravel())
fifa_df.loc[:, ['Man of the Match']] = moth_encoded

#dummy
fifa_onehot_df = pd.get_dummies(fifa_df, columns=['Team', 'Opponent', 'Round', 'PSO'])

pickle.dump((fifa_onehot_df, yn_encoder), open('data/fifa_clean.pickle', 'wb'))

"""
['Date', 'Team', 'Opponent', 'Goal Scored', 'Ball Possession %',
       'Attempts', 'On-Target', 'Off-Target', 'Blocked', 'Corners', 'Offsides',
       'Free Kicks', 'Saves', 'Pass Accuracy %', 'Passes',
       'Distance Covered (Kms)', 'Fouls Committed', 'Yellow Card',
       'Yellow & Red', 'Red', 'Man of the Match', '1st Goal', 'Round', 'PSO',
       'Goals in PSO', 'Own goals', 'Own goal Time']

Date                       object
Team                       object
Opponent                   object
Goal Scored                 int64
Ball Possession %           int64
Attempts                    int64
On-Target                   int64
Off-Target                  int64
Blocked                     int64
Corners                     int64
Offsides                    int64
Free Kicks                  int64
Saves                       int64
Pass Accuracy %             int64
Passes                      int64
Distance Covered (Kms)      int64
Fouls Committed             int64
Yellow Card                 int64
Yellow & Red                int64
Red                         int64
Man of the Match           object
1st Goal                  float64
Round                      object
PSO                        object
Goals in PSO                int64
Own goals                 float64
Own goal Time             float64
dtype: object
"""

