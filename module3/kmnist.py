import keras

import numpy as np

from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense, Activation
from keras.callbacks import TensorBoard

from keras.utils import to_categorical

from keras import backend as K

#Training settings
BATCH_SIZE = 128
NUM_CLASSES = 10
EPOCHS = 30

IMG_ROWS = 28
IMG_COLS = 28

(X_train, y_train), (X_test, y_test) = mnist.load_data()

n_sample, IMG_ROWS, IMG_COLS = X_train.shape

input_dim = IMG_ROWS * IMG_COLS
X_train = X_train.reshape(n_sample, input_dim).astype(np.float32) / 255
X_test = X_test.reshape(y_test.shape[0], input_dim).astype(np.float32) / 255

#convert to one hot
"""
5 [0. 0. 0. 0. 0. 1. 0. 0. 0. 0.]
"""
y_train_cat = to_categorical(y_train, NUM_CLASSES)
y_test_cat = to_categorical(y_test, NUM_CLASSES)


model = Sequential()
#model.add(Dense(NUM_CLASSES, input_dim=input_dim, activation='softmax'))
#or
model.add(Dense(NUM_CLASSES, input_dim=input_dim,  name="h1"))
model.add(Activation('relu'))
model.add(Dense(NUM_CLASSES, input_dim=input_dim, activation='softmax', name="h2"))
model.summary()
"""
Using TensorFlow backend.
_________________________________________________________________
Layer (type)                 Output Shape              Param #   
=================================================================
dense_1 (Dense)              (None, 10)                7850      
=================================================================
Total params: 7,850
Trainable params: 7,850
Non-trainable params: 0
_________________________________________________________________
"""

tb = TensorBoard(log_dir='./logs', histogram_freq=0, write_graph=True, write_images=True, \
      batch_size=BATCH_SIZE)

model.compile(optimizer='sgd', loss='categorical_crossentropy', metrics=['accuracy'])

history = model.fit(X_train, y_train_cat, batch_size=BATCH_SIZE, epochs=EPOCHS, verbose=1, \
   validation_data=(X_test, y_test_cat), callbacks=[tb])

score = model.evaluate(X_test, y_test_cat, verbose=0)
print('Test score:', score[0])
print('Test accuracy:', score[1])
print('score ', score)
