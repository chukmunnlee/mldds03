import os, time
import numpy as np

from matplotlib import pyplot as plt

from keras.models import Sequential
from keras.callbacks import TensorBoard, EarlyStopping
from keras.layers import Conv2D, MaxPool2D, Activation, BatchNormalization, Flatten, Dense
from keras.preprocessing.image import ImageDataGenerator

#data path
data_path = 'data/food'

batch_size=1

#labels
labels = [ 'chapati', 'fishball_noodle', 'satay' ]

data_train = {

      'chapati': [ 'chapati001.jpg', 'chapati002.jpg', 'chapati003.jpg', 'chapati004.jpg', \
            'chapati005.jpg', 'chapati006.jpg', 'chapati007.jpg', 'chapati008.jpg', \
            'chapati009.jpg', 'chapati010.jpg', 'chapati011.jpg', 'chapati012.jpg', \
            'chapati013.jpg', 'chapati014.jpg', 'chapati015.jpg' ],

      'fishball_noodle': [ 'fishball_noodle001.jpg', 'fishball_noodle002.jpg', \
            'fishball_noodle003.jpg', 'fishball_noodle004.jpg', 'fishball_noodle005.jpg', \
            'fishball_noodle006.jpg', 'fishball_noodle007.jpg', 'fishball_noodle008.jpg', \
            'fishball_noodle009.jpg', 'fishball_noodle010.jpg', 'fishball_noodle011.jpg', \
            'fishball_noodle012.jpg', 'fishball_noodle013.jpg', 'fishball_noodle014.jpg', \
            'fishball_noodle015.jpg' ],

      'satay': [ 'satay001.jpg', 'satay002.jpg', 'satay003.jpg', 'satay004.jpg', \
            'satay005.jpg', 'satay006.jpg', 'satay007.jpg', 'satay008.jpg', 'satay009.jpg', \
            'satay010.jpg', 'satay011.jpg', 'satay012.jpg', 'satay013.jpg', \
            'satay014.jpg', 'satay015.jpg' ] 
      }


data_validate = {
      'chapati': [ 'chapati001.jpg', 'chapati002.jpg', 'chapati003.jpg', \
            'chapati004.jpg', 'chapati005.jpg' ],

      'fishball_noodle': [ 'fishball_noodle001.jpg', 'fishball_noodle002.jpg', \
            'fishball_noodle003.jpg', 'fishball_noodle004.jpg', 'fishball_noodle005.jpg' ],

      'satay': [ 'satay001.jpg', 'satay002.jpg', 'satay003.jpg', 'satay004.jpg', 'satay005.jpg' ]
      }

def load_images(folder='train'):
   img_dir = os.path.join(data_path, folder)
   data_img = data_validate if 'validate' == folder else data_train
   pics = []
   for i in labels:
      root = os.path.join(img_dir, i)
      for j in data_img[i]:
         pics.append(os.path.join(root, j))
   return pics

def create_model(w=160, h=160, c=3):

   model = Sequential()

   #CNN Block 1
   #depth 8, kernel 3, stride 1, with padding
   #input shape: w, h, c
   #output shape: 53, 53, 8
   model.add(Conv2D( \
         filters=8, kernel_size=(3, 3), padding='same', \
         activation='relu', input_shape=(w, h, c)))
   model.add(MaxPool2D(pool_size=(3, 3)))

   #CNN Block 2
   #depth 16, kernel 3, stride 1, with padding
   #input shape 53, 53, 8
   #output shape: 26, 26, 16
   #batch norm is inserted before activation for 2nd conv block onwards
   #batch norm removes noise in the covariates (means, variances) when 
   #we stack convolutional networks
   model.add(Conv2D(filters=16, kernel_size=(3, 3), padding='same'))
   model.add(BatchNormalization())
   model.add(Activation('relu'))
   model.add(MaxPool2D(pool_size=(2, 2)))

   #CNN Block 3
   #depth 32, kernel 3, stride 1, with padding
   #input shape: 26, 26, 16
   #output shape: 13, 13, 32
   model.add(Conv2D(filters=32, kernel_size=(3, 3), padding='same'))
   model.add(BatchNormalization())
   model.add(Activation('relu'))
   model.add(MaxPool2D(pool_size=(2, 2)))

   #CNN Block 4
   #depth 32, kernel 3, stride 1, with padding
   #input shape: 13, 13, 32
   #output shape: 6, 6, 32
   model.add(Conv2D(filters=32, kernel_size=(3, 3), padding='same'))
   model.add(BatchNormalization())
   model.add(Activation('relu'))
   model.add(MaxPool2D(pool_size=(2, 2)))

   #Classifier
   #input shape: 6, 6,32
   #output shape: 3
   model.add(Flatten())
   model.add(Dense(len(labels), activation='softmax'))

   return model

train_data = load_images('train')
validate_data = load_images('validate')

print('Number of classes %d' %len(labels))
print('Train data: %d' %len(train_data))
print('Validate data: %d' %len(validate_data))
 
width = height = 160
channels = 3

datagen = ImageDataGenerator(rescale=1./255, rotation_range=5, zoom_range=.2, horizontal_flip=True)

generator = datagen.flow_from_directory(os.path.join(data_path, 'train'), color_mode='rgb', \
      target_size=(width, height), batch_size=batch_size, class_mode='categorical', shuffle=True)

model = create_model()

model.summary()

log_index = int(time.time())

tensorboard = TensorBoard(log_dir='./logs/baseline_cnn/%s' %log_index, \
      histogram_freq=0, write_graph=True, write_images=False)

early_stop = EarlyStopping(monitor='loss', patience=0, verbose=0, mode='auto')

model.compile(optimizer='adadelta', loss='categorical_crossentropy', metrics=['accuracy'])

model.fit_generator(generator, len(train_data)//batch_size, epochs=15, \
      callbacks=[tensorboard, early_stop])

"""
for i in range(4):
   x, y = next(generator)
   plt.imshow(x[0])
   plt.title('%d, %s' %(i, str(y[0])))
   plt.show()
"""
