import sys, os, random

import numpy as np

from PIL import Image
from yolo import YOLO

from matplotlib import pyplot as plt

if len(sys.argv) <= 1:
   print('Missing file name')
   sys.exit(-1)

img = Image.open(sys.argv[1])

yolo = YOLO()
#use the modified YOLO to get the predictions from the image
#image, bounding boxes, predicted class
pred_img, bb, scores, pred_classes = yolo.detect_image(img)

print(pred_classes)

clips = []

pres_img = Image.open(sys.argv[1])

for i, box in enumerate(bb):
   #t, l, b, r
   ty, tx, by, bx = box
   w = int(abs(bx - tx))
   h = int(abs(by - ty))
   arr = np.array(pres_img)
   tx = max(0, np.floor(tx + 0.5).astype('int32')) 
   ty = max(0, np.floor(ty + 0.5).astype('int32'))
   bx = min(img.size[0], np.floor(bx + 0.5).astype('int32'))
   by = min(img.size[1], np.floor(by + 0.5).astype('int32'))

   print(i, ty, ty + h, tx, tx + h, scores[i], pred_classes[i])

   clips.append(arr[ty: ty + h, tx: tx + w, :])

yolo.close_session()

plt.imshow(pred_img)
plt.show()

idx = np.arange(len(clips))

random.shuffle(idx)

size = 9 if len(clips) >= 10 else len(clips)

fig = plt.figure()
pidx = 330

for i in range(size):
   pr_idx = idx[i]
   pidx += 1
   ax = fig.add_subplot(pidx)
   ax.set_title('%s %0.2f' %(pred_classes[pr_idx], scores[pr_idx]))
   ax.imshow(clips[pr_idx])

plt.show()
