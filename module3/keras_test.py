import keras
import numpy as np

from PIL import Image

from matplotlib import pyplot as plt
from cairosvg import svg2png

from keras.applications import resnet50
from keras.applications import mobilenet
from keras.utils.vis_utils import model_to_dot

print(keras.__version__)

#restnet_model = resnet50.ResNet50(weights='imagenet')
#restnet_model.summary()

mobilenet_model = mobilenet.MobileNet(weights='imagenet')
#mobilenet_model.summary()

layer_svg = model_to_dot(mobilenet_model, show_shapes=True).create(prog='dot', format='svg')

svg2png(bytestring=layer_svg, write_to='data/mobilenet.png')

img = np.array(Image.open(open('mobilenet.png', 'rb')))

fig = plt.figure()
ax = fig.add_subplot(111)
ax.imshow(img)

plt.show()
