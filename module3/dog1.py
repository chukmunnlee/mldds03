import numpy as np
from PIL import Image
from matplotlib import pyplot as plt
from scipy.signal import convolve2d

from skimage.measure import block_reduce

from keras.activations import tanh

def sigmoid(x):
   return (1 + np.exp(-x)) ** -1

def relu(x):
   return np.maximum(x, 0)

def leaky_relu(x):
   return np.where(x > 0, x, x * 0.01)

def max_pool(x, pool_size=(2, 2)):
   return block_reduce(x, pool_size, np.max)

def average_pool(x, pool_size=(2, 2)):
   return block_reduce(x, pool_size, np.mean)

#dog = np.array(Image.open(open('data/dog.jpg', 'rb')))
img_grey = Image.open(open('data/dog.jpg', 'rb')).convert(mode='L')
#img_grey = Image.open(open('data/bcmb.jpg', 'rb')).convert(mode='L')

#kernel to detect edges
kernel = np.array([[-1, -1, -1], [-1, 8, -1], [-1, -1, -1]])

edges = convolve2d(img_grey, kernel, mode='valid')

print('shape = ', edges.shape)

fig = plt.figure()
ax = fig.add_subplot(121)

ax.imshow(img_grey)

ax = fig.add_subplot(122)

ax.imshow(max_pool(edges, pool_size=(5, 5)))

plt.show();
