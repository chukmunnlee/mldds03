import requests
import numpy as np
from PIL import Image, ImageOps

from matplotlib import pyplot as plt

r = requests.get(\
      'https://thumbs.dreamstime.com/b/indian-chapati-bread-isolated-white-background-33601745.jpg', \
      stream=True)
img = Image.open(r.raw)

img_crop = ImageOps.fit(img, (160, 160))

print('img = ', np.array(img).shape)
print('img_crop = ', np.array(img_crop).shape)

x = np.array(img_crop)
x = np.expand_dims(x, axis=0)

print(x.shape)

fig = plt.figure()
ax = fig.add_subplot(211)
ax.imshow(img)
ax = fig.add_subplot(212)
ax.imshow(img_crop)

plt.show()

